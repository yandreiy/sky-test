#ifndef CHANNELTYPE
#define CHANNELTYPE

enum class ChannelType
{
    SPORTS,
    KIDS,
    MUSIC,
    NEWS,
    MOVIES,
    None
};

inline uint qHash( ChannelType type, uint seed)
{
    return qHash( static_cast<uint>(type), seed);
}

#endif // CHANNELTYPE

