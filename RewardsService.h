#ifndef REWARDSSERVICE_H
#define REWARDSSERVICE_H

#include <QSet>
#include "ChannelType.h"
#include "CustomerAccount.h"
#include "CustomerRewardType.h"
#include "EligibilityService.h"

class RewardsService
{
public:
    /**
     * @brief RewardsService
     * @param eligibiltyService The service to check for eligibility. Ownership transfered.
     */
    RewardsService(EligibilityService* eligibiltyService);

    ~RewardsService();

    /**
     * @brief getAvailableRewards
     * @param customerNumber
     * @param channels
     * @return Collection of reward types.
     */
    QSet<CustomerRewardType> getAvailableRewards( const CustomerNumber& customerNumber, const QSet<ChannelType>& channels);

private:

    /**
     * @brief getRewardForChannel
     * @param channel
     * @return The reward associated with the channel
     */
    CustomerRewardType getRewardForChannel( ChannelType channel);

    EligibilityService* m_eligibiltyService;
};

#endif // REWARDSSERVICE_H
