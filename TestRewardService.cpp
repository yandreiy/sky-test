#include <QtTest/QtTest>

#include <QScopedPointer>
#include "RewardsService.h"
#include "EligibilityServiceMock.h"

class TestRewardService: public QObject
{
    Q_OBJECT

private slots:

    void initTestCase()
    {
        eligibilityServiceMock = new EligibilityServiceMock();
        rewardService.reset( new RewardsService(eligibilityServiceMock));
        channels << ChannelType::KIDS << ChannelType::MOVIES << ChannelType::SPORTS;
    }

    void test_InvalidCustomerNumberReturnsEmptyResult();
    void test_IneligibleCustomerReturnsEmptyResult();
    void test_EligibleCustomerReturnsNonEmptyResult();

private:
    QScopedPointer<RewardsService> rewardService;
    EligibilityServiceMock* eligibilityServiceMock;
    QSet<ChannelType> channels;
};

void TestRewardService::test_InvalidCustomerNumberReturnsEmptyResult()
{
    eligibilityServiceMock->set_fake_result(
                EligibilityService::ErrorCode::InvalidAccountNumber,
                EligibilityService::CustomerEligibleType::CUSTOMER_INELIGIBLE
                );

    auto collection = rewardService->getAvailableRewards("some-customer-number", channels);
    QVERIFY( collection.empty());
}

void TestRewardService::test_IneligibleCustomerReturnsEmptyResult()
{
    eligibilityServiceMock->set_fake_result(
                EligibilityService::ErrorCode::None,
                EligibilityService::CustomerEligibleType::CUSTOMER_INELIGIBLE
                );

    auto collection = rewardService->getAvailableRewards("some-customer-number", channels);
    QVERIFY( collection.empty());
}

void TestRewardService::test_EligibleCustomerReturnsNonEmptyResult()
{
    eligibilityServiceMock->set_fake_result(
                EligibilityService::ErrorCode::None,
                EligibilityService::CustomerEligibleType::CUSTOMER_ELIGIBLE
                );

    auto collection = rewardService->getAvailableRewards("some-customer-number", channels);
    QVERIFY( !collection.empty());
}

/// More tests can be added for cases such as:
/// Eligible customer, empty channels -> empty result
/// Eligible customer, spefic channel -> specific reward
/// etc

QTEST_MAIN(TestRewardService)
#include "TestRewardService.moc"
