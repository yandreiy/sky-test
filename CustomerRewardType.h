#ifndef CUSTOMERREWARDTYPE
#define CUSTOMERREWARDTYPE

enum class CustomerRewardType
{
    CHAMPIONS_LEAGUE_FINAL_TICKET,
    KARAOKE_PRO_MICROPHONE,
    PIRATES_OF_THE_CARIBBEAN_COLLECTION,
    None
};

inline uint qHash( CustomerRewardType type, uint seed)
{
    return qHash( static_cast<uint>(type), seed);
}

#endif // CUSTOMERREWARDTYPE

