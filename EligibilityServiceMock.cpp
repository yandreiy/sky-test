#include "EligibilityServiceMock.h"

EligibilityService::ErrorCode EligibilityServiceMock::get_eligibility(const CustomerNumber &/*customerNumber*/, EligibilityService::CustomerEligibleType &eligibility)
{
    eligibility = m_eligibleType;
    return m_error;
}

void EligibilityServiceMock::set_fake_result(EligibilityService::ErrorCode error, EligibilityService::CustomerEligibleType eligibleType)
{
    m_error = error;
    m_eligibleType = eligibleType;
}
