#ifndef ELIGIBILITYSERVICEMOCK_H
#define ELIGIBILITYSERVICEMOCK_H

#include "EligibilityService.h"

class EligibilityServiceMock: public EligibilityService
{
    // EligibilityService interface
public:
    virtual ErrorCode get_eligibility(const CustomerNumber &customerNumber, CustomerEligibleType &eligibility);

    /// The mock method
    void set_fake_result(ErrorCode error, CustomerEligibleType eligibleType);

private:
    ErrorCode m_error;
    CustomerEligibleType m_eligibleType;
};

#endif // ELIGIBILITYSERVICEMOCK_H
