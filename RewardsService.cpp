#include "RewardsService.h"

RewardsService::RewardsService(EligibilityService *eligibiltyService)
    :m_eligibiltyService(eligibiltyService)
{
    Q_ASSERT (eligibiltyService != nullptr);
}

RewardsService::~RewardsService()
{
    delete m_eligibiltyService;
}

QSet<CustomerRewardType> RewardsService::getAvailableRewards(const CustomerNumber &customerNumber, const QSet<ChannelType> &channels)
{
    EligibilityService::CustomerEligibleType eligibility;
    EligibilityService::ErrorCode error = m_eligibiltyService->get_eligibility(customerNumber, eligibility);

    if (error != EligibilityService::ErrorCode::None) {
        /// handle the error
        /// return empty results
        return  QSet<CustomerRewardType>();
    }

    if (eligibility == EligibilityService::CustomerEligibleType::CUSTOMER_INELIGIBLE) {
        /// handle
        /// return empty results
        return  QSet<CustomerRewardType>();
    }

    QSet<CustomerRewardType> rewards;

    for ( ChannelType ch: channels) {
        CustomerRewardType reward = getRewardForChannel(ch);
        if ( reward != CustomerRewardType::None) {
            rewards.insert(reward);
        }
    }

    return rewards;
}

CustomerRewardType RewardsService::getRewardForChannel(ChannelType channel)
{
    switch (channel) {
    case ChannelType::SPORTS:
        return CustomerRewardType::CHAMPIONS_LEAGUE_FINAL_TICKET;
    case ChannelType::KIDS:
        return CustomerRewardType::None;
    case ChannelType::MUSIC:
        return CustomerRewardType::KARAOKE_PRO_MICROPHONE;
    case ChannelType::NEWS:
        return CustomerRewardType::None;
    case ChannelType::MOVIES:
        return CustomerRewardType::PIRATES_OF_THE_CARIBBEAN_COLLECTION;
    default:
        return CustomerRewardType::None;
    }
}

