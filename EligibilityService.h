#ifndef ELIGIBILITYSERVICE
#define ELIGIBILITYSERVICE

#include <CustomerAccount.h>

/// Interface for Eligibility Service
class EligibilityService
{
public:

    /// Enum with the eligiblity response
    enum class CustomerEligibleType
    {
        CUSTOMER_ELIGIBLE,
        CUSTOMER_INELIGIBLE
    };

    /// Enum containing all possible errors that might occur while checking for eligibility
    enum class ErrorCode
    {
        None,
        TechnicalFailure,
        InvalidAccountNumber
    };

    virtual ~EligibilityService() {  }

    /**
     * @brief get_eligibility
     * @param customerNumber
     * @param eligibility
     * @return
     */
    virtual ErrorCode get_eligibility( const CustomerNumber& customerNumber, CustomerEligibleType& eligibility ) = 0;

};

#endif // ELIGIBILITYSERVICE

