#ifndef CUSTOMERACCOUNT
#define CUSTOMERACCOUNT

#include <QString>

class CustomerAccount
{
public:
    /**
     * @brief customerNumber
     * @return The customer number of this account
     */
    QString customerNumber() { return m_customerNumber; }

private:
    QString m_customerNumber; /// This is set/generated at the creation of a customer account
};

typedef QString CustomerNumber; /// Account Number or Id

#endif // CUSTOMERACCOUNT

