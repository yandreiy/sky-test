QT += core
QT -= gui

TARGET = Sky-test
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

QT += testlib
TEMPLATE = app

SOURCES += main.cpp \
    RewardsService.cpp \
    EligibilityServiceMock.cpp \
    TestRewardService.cpp

HEADERS += \
    RewardsService.h \
    ChannelType.h \
    CustomerRewardType.h \
    EligibilityService.h \
    CustomerAccount.h \
    EligibilityServiceMock.h
